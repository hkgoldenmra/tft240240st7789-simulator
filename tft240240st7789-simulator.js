function TFT240240ST7789Simulator() {
	var VIEW_WIDTH = 240;
	var VIEW_HEIGHT = 240;
	var SCREEN_WIDTH = 240;
	var SCREEN_HEIGHT = 320;
	var MAX_WIDTH = 240;
	var MAX_HEIGHT = 512;
	var MAX_DUTY_CYCLE = 8;
	var pwPin = document.getElementById("pw");
	var rtPin = document.getElementById("rt");
	var ssPin = document.getElementById("ss");
	var dcPin = document.getElementById("dc");
	var daPin = document.getElementById("da");
	var ckPin = document.getElementById("ck");
	var dutyCycle = 0;
	var bufferByte = 0;
	var controlByte = 0;
	var dataByte = [];
	var sleep = true;
	var negative = true;
	var display = false;
	var idle = true;
	var line = 0;
	var x1 = 0;
	var x2 = 0;
	var y1 = 0;
	var y2 = 0;
	var padChar = function (string, length, char) {
		string = "" + string;
		while (string.length < length) {
			string = char + string;
		}
		return string;
	};
	var replaceAll = function (string, search, replace) {
		while (string.indexOf(search) > -1) {
			string = string.replace(search, replace);
		}
		return string;
	};
	var updatePowerInfo = function (pins) {
		document.getElementById("pw-info").innerHTML
			= "Power : "
			+ (isPinHigh(pins) ? "ON" : "OFF");
	};
	var updateResetInfo = function (pins) {
		document.getElementById("rt-info").innerHTML
			= "Reset : "
			+ (!isPinHigh(pins) ? "ON" : "OFF");
	};
	var updateInfo = function () {
		updatePowerInfo(pwPin);
		updateResetInfo(rtPin);
	};
	var updateDDRAM = function () {
		updateInfo();
	};
	var reset = function () {
	};
	var writeData = function (data) {
	};
	var writeInstruction = function (data) {
	};
	var isPinHigh = function (pin) {
		return (pin.getAttribute("opacity") == "1");
	};
	var setPin = function (pin, status) {
		pin.setAttribute("opacity", (status ? "1" : "0"));
		document.getElementById(pin.id + "-back").setAttribute("xlink:href", "#pin-" + (status ? "high" : "low"));
		document.getElementById(pin.id + "-text").setAttribute("fill", "#" + (status ? "008800" : "880000"));
		updateInfo();
	};
	var fillDraw = function (x, y, width, height, color) {
		var template = document.getElementById("template");
		var element = template.cloneNode(true);
		element.removeAttribute("id");
		element.setAttribute("class", "addition");
		element.setAttribute("x", x);
		element.setAttribute("y", y);
		element.setAttribute("width", width);
		element.setAttribute("height", height);
		element.setAttribute("fill", color);
		template.parentNode.appendChild(element);
	};
	this.pw = function () {
		setPin(pwPin, !isPinHigh(pwPin));
		var additions = document.getElementsByClassName("addition");
		while (additions.length > 0) {
			additions[0].remove();
		}
	};
	this.rt = function () {
		setPin(rtPin, !isPinHigh(rtPin));
	};
	this.ss = function () {
		setPin(ssPin, !isPinHigh(ssPin));
		if (isPinHigh(ssPin)) {
			if ((controlByte & 0x38) == 0x38) {
				console.log("Idle : " + ((controlByte & 0x01) == 0x01 ? "ON" : "OFF"));
			} else if ((controlByte & 0x2C) == 0x2C) {
				console.log("Set Pixel Color : ");
				for (var y = y1; y <= y2; y++) {
					for (var x = x1; x <= x2; x++) {
						fillDraw(x, y, 1, 1, "#FF0000");
					}
				}
			} else if ((controlByte & 0x2B) == 0x2B) {
				console.log("Set Y1 : " + (y1 = ((dataByte[0] << 8) | dataByte[1])));
				console.log("Set Y2 : " + (y2 = ((dataByte[2] << 8) | dataByte[3])));
			} else if ((controlByte & 0x2A) == 0x2A) {
				console.log("Set X1 : " + (x1 = ((dataByte[0] << 8) | dataByte[1])));
				console.log("Set X2 : " + (x2 = ((dataByte[2] << 8) | dataByte[3])));
			} else if ((controlByte & 0x28) == 0x28) {
				console.log("Screen : " + ((controlByte & 0x01) == 0x01 ? "ON" : "OFF"));
			} else if ((controlByte & 0x20) == 0x20) {
				console.log("Negative Mode : " + ((controlByte & 0x01) == 0x01 ? "ON" : "OFF"));
			} else if ((controlByte & 0x10) == 0x10) {
				console.log("Sleep Mode : " + ((controlByte & 0x01) == 0x01 ? "ON" : "OFF"));
			} else if ((controlByte & 0x01) == 0x01) {
				console.log("Software Reset");
			}
			controlByte = 0;
			dataByte = [];
		}
	};
	this.dc = function () {
		setPin(dcPin, !isPinHigh(dcPin));
	};
	this.da = function () {
		setPin(daPin, !isPinHigh(daPin));
	};
	this.ck = function () {
		setPin(ckPin, !isPinHigh(ckPin));
		if (isPinHigh(pwPin) && isPinHigh(rtPin) && !isPinHigh(ssPin) && !isPinHigh(ckPin)) {
			dutyCycle++;
			if (isPinHigh(daPin)) {
				bufferByte |= (1 << (MAX_DUTY_CYCLE - dutyCycle));
			}
			console.log("Duty Cycle : " + dutyCycle);
			dutyCycle %= 8;
			if (dutyCycle == 0) {
				if (isPinHigh(dcPin)) {
					dataByte.push(bufferByte);
					console.log("Data Byte = B" + padChar(bufferByte.toString(2), 8, "0"));
				} else {
					controlByte = bufferByte;
					console.log("Control Byte = B" + padChar(bufferByte.toString(2), 8, "0"));
				}
				bufferByte = 0;
			}
		}
	};
}

window.addEventListener("load", function (loadEvent) {
	var simulator = new TFT240240ST7789Simulator();
	document.getElementById("pw").addEventListener("click", function (clickEvent) {
		simulator.pw();
	});
	document.getElementById("rt").addEventListener("click", function (clickEvent) {
		simulator.rt();
	});
	document.getElementById("ss").addEventListener("click", function (clickEvent) {
		simulator.ss(1);
	});
	document.getElementById("dc").addEventListener("click", function (clickEvent) {
		simulator.dc(0);
	});
	document.getElementById("da").addEventListener("click", function (clickEvent) {
		simulator.da(0);
	});
	document.getElementById("ck").addEventListener("click", function (clickEvent) {
		simulator.ck();
	});
});